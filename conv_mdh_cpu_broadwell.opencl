__kernel void mcc_nhwc_krsc_npqk_stride_2_asymmetrical_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[(1 * 1) *
                                    (1 * 1) *
                                    (2 * 7) *
                                    (1 * 1)];
    __private float filter_prv[(1) *
                                       (1) *
                                       (1) *
                                       (1)];
    {
    const size_t wg_2 = get_group_id(0) / (1 * 8) % (112); {
    const size_t wg_3 = get_group_id(0) / (1) % (8); {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_4 = get_local_id(0) % (32); {
    {
    {
    {
    {
    {
    {
    for (size_t glb_4 = 0; glb_4 < 2; ++glb_4) {
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
    {
        out_prv[(0 * 1 + 0) * (1 * 1) * (2 * 7) * (1 * 1) + (0 * 1 + 0) * (2 * 7) * (1 * 1) + (lcl_3 * 7 + prv_3) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}
    }
    {
    for (size_t glb_6 = 0; glb_6 < 7; ++glb_6) {
    {
        for (size_t lcl_5 = 0; lcl_5 < 3; ++lcl_5) {
        {
        for (size_t lcl_7 = 0; lcl_7 < 7; ++lcl_7) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
        {
            {
               
               
               
               
                    filter_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
                                      =
                    filter_glb[(0 * 2 * 32 * 1 * 1 + glb_4 * 32 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) * (7) * (7) * (3) + (0 * 7 * 1 * 1 * 1 + glb_6 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (7) * (3) + (0 * 1 * 1 * 7 * 1 + 0 * 1 * 7 * 1 + 0 * 7 * 1 + lcl_7 * 1 + 0) * (3) + (0 * 1 * 1 * 3 * 1 + 0 * 1 * 3 * 1 + 0 * 3 * 1 + lcl_5 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            {
            {
            for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
            {
            out_prv[(0 * 1 + 0) * (1 * 1) * (2 * 7) * (1 * 1) + (0 * 1 + 0) * (2 * 7) * (1 * 1) + (lcl_3 * 7 + prv_3) * (1 * 1) + (0 * 1 + 0) ]
                +=
            images_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((2 * 112 + 7 - 1)) * ((2 * 112 + 7 - 1)) * (3) + ((wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * 2 + (0 * 7 * 1 * 1 * 1 + glb_6 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 + 7 - 1)) * (3) + ((wg_3 * 1 * 1 * 2 * 7 + 0 * 1 * 2 * 7 + 0 * 2 * 7 + lcl_3 * 7 + prv_3) * 2 + (0 * 1 * 1 * 7 * 1 + 0 * 1 * 7 * 1 + 0 * 7 * 1 + lcl_7 * 1 + 0)) * (3) + (0 * 1 * 1 * 3 * 1 + 0 * 1 * 3 * 1 + 0 * 3 * 1 + lcl_5 * 1 + 0) ]
            *
            filter_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
    {
        out_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (112) * (64) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (64) + (wg_3 * 1 * 1 * 2 * 7 + 0 * 1 * 2 * 7 + 0 * 2 * 7 + lcl_3 * 7 + prv_3) * (64) + (0 * 2 * 32 * 1 * 1 + glb_4 * 32 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) ]
                                                 =
        out_prv[(0 * 1 + 0) * (1 * 1) * (2 * 7) * (1 * 1) + (0 * 1 + 0) * (2 * 7) * (1 * 1) + (lcl_3 * 7 + prv_3) * (1 * 1) + (0 * 1 + 0) ]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
