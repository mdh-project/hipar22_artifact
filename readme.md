# Expressing Hierarchical Code Optimizations via MDH-Based Schedules

This is a preliminary version of our artifact implementation which contains the implementations introduced and discussed in the paper  Expressing Hierarchical Code Optimizations via MDH-Based Schedules under review at [Workshop on Hierarchical Parallelism for Exascale Computing (HiPar'22)](https://www.hipar.net/).

All experiments in our paper haven been conducted on NVIDIA A100-PCIE-40GB & NVIDIA Tesla V100-SXM2-16GB GPU and Intel(R) Xeon(R) CPU E5-2683 v4 @ 2.10GHz (HyperThreading disabled).

In case of any problems, please feel free to open an issue to get in touch with the authors.

**Note: If our paper gets accepted for publication, we will provide a fully functional artifact that contains scripts for conveniently reproducing and plotting all experiments presented in the paper (including experimental results of our competitors TVM, cuBLAS, and oneMKL).**