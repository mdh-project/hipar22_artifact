__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    {
    const size_t wg_2 = get_group_id(0); {
    {
    {
    const size_t wi_2 = get_local_id(0); {
    {
    {
    {
    {
    {
    {
    {
    {
        c_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (1000) + (0 * 125 * 1 * 8 * 1 + wg_2 * 1 * 8 * 1 + 0 * 8 * 1 + wi_2 * 1 + 0) ]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 256; ++glb_3) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 4; ++lcl_3) {
            {
            {
            for (size_t prv_3 = 0; prv_3 < 2; ++prv_3) {
            c_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (1000) + (0 * 125 * 1 * 8 * 1 + wg_2 * 1 * 8 * 1 + 0 * 8 * 1 + wi_2 * 1 + 0) ]
                +=
            a_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (2048) + (glb_3 * 1 * 4 * 1 * 2 + 0 * 4 * 1 * 2 + lcl_3 * 1 * 2 + 0 * 2 + prv_3) ]
            *
            b_glb[(glb_3 * 1 * 4 * 1 * 2 + 0 * 4 * 1 * 2 + lcl_3 * 1 * 2 + 0 * 2 + prv_3) * (1000) + (0 * 125 * 1 * 8 * 1 + wg_2 * 1 * 8 * 1 + 0 * 8 * 1 + wi_2 * 1 + 0) ]
            ;
            }}}
        }}}
    }
    }}
    }}}
    }}}
}
